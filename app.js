
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3001);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

//Controller
app.get('/', routes.index);
app.get('/users', user.list);
app.get('/create', routes.create);
app.get('/child', routes.child);

server = http.createServer(app);
//http.createServer(app).listen(app.get('port'), function(){
server.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

//SocketIO
var socketIO = require('socket.io');

var io = socketIO.listen(server);

//Clients {screenId:socket.id}
var childs = {};

io.sockets.on('connection', function(socket) {
  console.log("connection");

	socket.on('create', function(data){
		console.log("create");
	});

  socket.on('message', function(data) {
    console.log("message");
    io.sockets.emit('message', { value: data.value });
  });

  socket.on("registerChild", function(data) {
    console.log("registerChild");
		if(childs[data.id]){
    	console.log("registerChild2");
    	childs[data.id].push(socket.id);
		}else{
    	console.log("registerChild1");
    	childs[data.id] = [socket.id];
		}
  });

  socket.on("p_to_s_emitChild", function(data) {
    console.log("p_to_s_emitChild");
    var childAry = childs[data.id];
		if(childAry instanceof Array){
			for(var i=0; i<childAry.length; i++){
    		io.sockets.socket(childAry[i]).emit("s_to_c_emitChild", {value:data.value,id:data.id});
			}
		}
  });

  socket.on('disconnect', function(){
    console.log("disconnect");
  });
});



