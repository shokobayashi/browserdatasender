/*
 * GET home page.
 */

exports.index = function(req, res){
  res.render('index', { title: 'Browser Data Sender' });
};
exports.create = function(req, res){
  res.render('create', { title: 'Browser Data Sender'});
};
exports.child = function(req, res){
  res.render('child', { title: 'child'});
};
